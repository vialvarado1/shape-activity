import unittest


from shape import Circulo
from shape import Triangulo
from shape import Cuadrado

class TestCirculo(unittest.TestCase):

    @classmethod
    def setUp(self) -> None:
        self.circulo = Circulo()

    def test_instance(self):


        self.assertNotEqual(self.circulo, None)

    def test_default_value(self):


        self.assertEqual(self.circulo.valor, 0)

    def test_area(self):

        self.circulo.area(3.1416, 2)

        self.assertEqual(self.circulo.valor, 12.5664)

    def test_perimetro(self):

        self.circulo.perimetro(3.1416, 2)
        self.assertEqual(self.circulo.valor, 12.5664)


class TestTriangulo(unittest.TestCase):

    @classmethod
    def setUp(self) -> None:
        self.triangulo = Triangulo()

    def test_instance(self):


        self.assertNotEqual(self.triangulo, None)

    def test_default_value(self):


        self.assertEqual(self.triangulo.valor, 0)

    def test_area(self):

        self.triangulo.area(3, 5, 4)

        self.assertEqual(self.triangulo.valor, 3.75)

    def test_perimetro(self):

        self.triangulo.perimetro(3, 5, 4)
        self.assertEqual(self.triangulo.valor, 12)

class TestCuadrado(unittest.TestCase):

    @classmethod
    def setUp(self) -> None:
        self.cuadrado = Cuadrado()

    def test_instance(self):


        self.assertNotEqual(self.cuadrado, None)

    def test_default_value(self):


        self.assertEqual(self.cuadrado.valor, 0)

    def test_area(self):

        self.cuadrado.area(2)

        self.assertEqual(self.cuadrado.valor, 4)

    def test_perimetro(self):

        self.cuadrado.perimetro(2)
        self.assertEqual(self.cuadrado.valor, 8)