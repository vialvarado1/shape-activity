from abc import ABC, abstractmethod


class Parametros(ABC):
    """interface"""

    @abstractmethod
    def on(self):
        pass

    def off(self):
        pass


class Circulo(Parametros):
    valor = 0

    def on(self):
        print("Se enciende")


    def off(self):
        print("Se apaga")

    def area(self, pi, r):
        self.valor =  pi*r**2

    def perimetro(self, pi, r):

         self.valor = 2*pi*r
class Triangulo(Parametros):
    valor = 0

    def on(self):
        print("Se enciende")


    def off(self):
        print("Se apaga")

    def area(self, a, b, c):
        self.valor = (a*b)/c

    def perimetro(self, a, b, c):

         self.valor = a + b + c

class Cuadrado(Parametros):
    valor = 0

    def on(self):
        print("Se enciende")


    def off(self):
        print("Se apaga")

    def area(self, a):
        self.valor = a*a

    def perimetro(self, a):

         self.valor = a+a+a+a

if __name__ == '__main__':
    c = Circulo
    c = Triangulo
